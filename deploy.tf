#
# Number of web instance
#
variable "count" {
  default = 2
}

#
# Create a network in our project
# project is defined in provider.tf
#
resource "openstack_networking_network_v2" "tf_network" {
  region = "nova"
  name = "tf_network"
  admin_state_up = "true"
}

#
# Create a subnet in our new network(s)
#
resource "openstack_networking_subnet_v2" "tf_net_sub1" {
  region = "nova"
  network_id = "${openstack_networking_network_v2.tf_network.id}"
  cidr = "3.3.33.0/24"
  dns_nameservers="${var.name_server}"
  ip_version = 4
}

#
# Create a router for our network(s)
# with NSX-T no need to create exclusive router
#
resource "openstack_networking_router_v2" "tf_router1" {
  region = "nova"
  name = "tf_router1"
  admin_state_up = "true"
  external_gateway = "${var.external-gw}"
}


#
# Attach the Router to our Network via an Interface
#
resource "openstack_networking_router_interface_v2" "tf_rtr_if_1" {
  region = "nova"
  router_id = "${openstack_networking_router_v2.tf_router1.id}"
  subnet_id = "${openstack_networking_subnet_v2.tf_net_sub1.id}"
  depends_on = [
      "openstack_networking_subnet_v2.tf_net_sub1",
  ]
}

#
# Create some Openstack Floating IP's for our VM's
#
resource "openstack_networking_floatingip_v2" "fip" {
  count = "${var.count}"
  region = "nova"
  pool = "${var.pub-pool}"
  depends_on = [
      "openstack_networking_router_interface_v2.tf_rtr_if_1",
  ]

}

resource "openstack_networking_floatingip_v2" "fipdb" {
  region = "nova"
  pool = "${var.pub-pool}"
  depends_on = [
      "openstack_networking_router_interface_v2.tf_rtr_if_1",
  ]
}


#
# Create security group.  allowing -
#   - ssh
#   - icmp
#   - mysql
#   - web
#
resource "openstack_compute_secgroup_v2" "terraform_sec" {
  name        = "allow_ssh"
  description = "my security group"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
  rule {
    from_port = 3306
    to_port = 3306
    ip_protocol = "tcp"
    cidr = "0.0.0.0/0"
  }

  rule {
    from_port = -1
    to_port = -1
    ip_protocol = "icmp"
    cidr = "0.0.0.0/0"
  }
}

#
# boots 2 instances of the web server in AZ zone defined in variable.tf
# remove availablity_zone or set to nova if single AZ
#   - dependency to subnet creation
#
resource "openstack_compute_instance_v2" "web" {
  count = "${var.count}"
  name = "${format("web-%02d", count.index+1)}"
  image_name = "${var.image-name}"
  availability_zone = "${var.az-zone}"
  flavor_id = "${var.flavor}"
  security_groups = ["${openstack_compute_secgroup_v2.terraform_sec.name}"]
  key_pair  = "${var.openstack_keypair}"
  network {
    name = "${openstack_networking_network_v2.tf_network.name}"
  }
  depends_on = [
    "openstack_networking_subnet_v2.tf_net_sub1",
    "openstack_networking_floatingip_v2.fip",
  ]
}

#
# boots 1 instance of the db server in AZ zone defined in variable.tf
# remove availablity_zone or set to nova if single AZ
#   - dependency to subnet creation
#
resource "openstack_compute_instance_v2" "db" {
  name = "db1"
  image_name = "${var.image-name}"
  availability_zone = "${var.az-zone}"
  flavor_id = "${var.flavor}"
  security_groups = ["${openstack_compute_secgroup_v2.terraform_sec.name}"]
  key_pair  = "${var.openstack_keypair}"
  network {
    name = "${openstack_networking_network_v2.tf_network.name}"
  }
  depends_on = [
     "openstack_networking_subnet_v2.tf_net_sub1",
     "openstack_compute_instance_v2.web",
  ]
}

#
#associate floating IP to DB server
#
resource "openstack_compute_floatingip_associate_v2" "fipdb" {
  floating_ip = "${openstack_networking_floatingip_v2.fipdb.address}"
  instance_id = "${openstack_compute_instance_v2.db.id}"
}

#
#associate floating IP to Web Server(s)
#
resource "openstack_compute_floatingip_associate_v2" "fip" {
  count = "${var.count}"
  floating_ip = "${element(openstack_networking_floatingip_v2.fip.*.address, count.index)}"
  instance_id = "${element(openstack_compute_instance_v2.web.*.id, count.index)}"
}

